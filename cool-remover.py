#!/usr/bin/env python3
import argparse
import os
import sys
from stat import S_ISDIR, S_ISREG

def guarded(dir_name):
    return 'full' in dir_name

def visit(directory, verbose=False):
    if guarded(directory) and not args.force:
        sys.stderr.write("WARNING: ignoring attempt to clean idiot-guarded " + directory + " folder\n")
        return

    if verbose:
        sys.stdout.write(directory)

    os.chdir(directory)

    files = []
    dirs = []

    for entry in os.listdir("."):
        entry_fullpath = (directory + "/" + entry).replace("//","/")
        stat = os.stat(entry_fullpath)
        mode = stat.st_mode
        if S_ISDIR(mode):
            dirs.append(entry_fullpath)
        elif S_ISREG(mode):
            files.append({'name': entry_fullpath, 'timestamp': stat.st_mtime})
        else:
            sys.stderr.write("WARNING:" + entry_fullpath + " is neither directory nor a regular file!")

    files.sort(key = lambda f: f['timestamp'])
    files_to_delete = files[:-1]
    for f in files_to_delete:
        if args.print_only:
            print(f['name'])
        else:
            os.unlink(f['name'])

    if verbose:
        sys.stdout.write('\r')

    if not args.only_one_level:
        for d in dirs:
            visit(d)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--print-only', action = 'store_true',
        help = "do not delete anything, only print which files are subject to deletion")
    parser.add_argument('-o', '--only-one-level', action = 'store_true',
        help = "do not recursively dive into further folders")
    parser.add_argument('-f', '--force', action = 'store_true',
        help = "clean even in guarded folders")
    parser.add_argument('-q', '--quiet', action = 'store_true',
        help = "do not print the currently cleared directory")
    parser.add_argument("path")
    args = parser.parse_args()

    root = os.getcwd() + "/" + args.path
    visit(root.replace("//", "/"), not args.quiet)

